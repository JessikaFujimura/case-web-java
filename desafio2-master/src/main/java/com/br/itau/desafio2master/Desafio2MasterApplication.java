package com.br.itau.desafio2master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Desafio2MasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(Desafio2MasterApplication.class, args);
	}

}
