package com.br.itau.desafio2master.Controller;

import com.br.itau.desafio2master.ApiRest.ApiRestFeign;
import com.br.itau.desafio2master.Model.Lancamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/lancamento")
public class LancamentoController {

    @Autowired
    private ApiRestFeign apiRestFeign;

    @GetMapping
    @ResponseBody
    public List<Lancamento> getLancamento(@RequestParam(required = false) Integer categoria){
        if(categoria != null){
            List<Lancamento> lancamentos = apiRestFeign.listarLancamentos();
            return lancamentos.stream().filter(i -> i.getCategoria() == categoria)
                    .collect(Collectors.toList());
        }
        return apiRestFeign.listarLancamentos();
    }

    @GetMapping("/{id}")
    public Lancamento getLancamentoPorId(@PathVariable("id") long id){
        List<Lancamento> lancamentos = apiRestFeign.listarLancamentos();
        return lancamentos.stream().filter(i -> i.getId() == id)
                .findFirst()
                .orElseThrow(() -> new NullPointerException("Lançamento não encontrado"));
    }

}
