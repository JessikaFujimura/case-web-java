package com.br.itau.desafio2master.ApiRest;


import com.br.itau.desafio2master.Model.Categoria;
import com.br.itau.desafio2master.Model.Lancamento;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(url = "${api.rest.url}", name = "rest-api")
public interface ApiRestFeign {

    @GetMapping("/lancamentos")
    List<Lancamento> listarLancamentos();

    @GetMapping("/categorias")
    List<Categoria> listarCategorias();
}
