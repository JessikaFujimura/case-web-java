package com.br.itau.desafio2master.Controller;

import com.br.itau.desafio2master.ApiRest.ApiRestFeign;
import com.br.itau.desafio2master.Model.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

    @Autowired
    private ApiRestFeign apiRestFeign;

    @GetMapping
    public List<Categoria> getCategorias(){
        return apiRestFeign.listarCategorias();
    }

    @GetMapping("/{id}")
    public Categoria getCategoriaPorId(@PathVariable("id") long id){
        List<Categoria> categorias = apiRestFeign.listarCategorias();
        return categorias.stream().filter(i -> i.getId() == id)
                .findFirst()
                .orElseThrow(() -> new NullPointerException("Categoria não encontrada"));
    }

}
