package com.br.itau.desafio2master.Model;


public class Lancamento {

    private long id;
    private double valor;
    private String origem;
    private int categoria;
    private int mesLancamento;

    public Lancamento() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public void setValor(double valor) {
        this.valor = valor;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }


    public void setMesLancamento(int mesLancamento) {
        this.mesLancamento = mesLancamento;
    }

    @Override
    public String toString() {
        return "Lancamento{" +
                "id=" + id +
                ", valor=" + valor +
                ", origem='" + origem + '\'' +
                ", categoria=" + categoria +
                ", mesLancamento=" + mesLancamento +
                '}';
    }
}
