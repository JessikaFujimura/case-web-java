package com.br.itau.desafio2master.Builder;

import com.br.itau.desafio2master.Model.Lancamento;

public class LancamentoBuilder {

    private Lancamento lancamento;

    private LancamentoBuilder(){}

    public static LancamentoBuilder umLancamento(){
        LancamentoBuilder builder = new LancamentoBuilder();
        builder.lancamento = new Lancamento();
        builder.lancamento.setId(1L);
        builder.lancamento.setValor(100.50);
        builder.lancamento.setOrigem("origem");
        builder.lancamento.setCategoria(1);
        builder.lancamento.setMesLancamento(1);
        return builder;
    }

    public Lancamento build(){
        return lancamento;
    }

}
