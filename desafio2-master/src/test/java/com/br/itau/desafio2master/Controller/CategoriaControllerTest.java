package com.br.itau.desafio2master.Controller;

import com.br.itau.desafio2master.ApiRest.ApiRestFeign;
import com.br.itau.desafio2master.Builder.CategoriaBuilder;
import com.br.itau.desafio2master.Model.Categoria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class CategoriaControllerTest {

    @InjectMocks
    private CategoriaController controller;

    @Mock
    private ApiRestFeign apiRestFeign;

    @Test
    public void testListarTodasAsCategorias(){
        List<Categoria> expectedResponse = List.of(CategoriaBuilder.umaCategoria().build(),
                CategoriaBuilder.umaCategoria().build());
        Mockito.when(apiRestFeign.listarCategorias()).thenReturn(expectedResponse);

        controller.getCategorias();

        Mockito.verify(apiRestFeign, Mockito.times(1)).listarCategorias();
    }


    @Test
    public void testObterUmaCategoriaPorId(){
        List<Categoria> lista = List.of(CategoriaBuilder.umaCategoria().build(),
                CategoriaBuilder.umaCategoria().build());
        Mockito.when(apiRestFeign.listarCategorias()).thenReturn(lista);

        Categoria response = controller.getCategoriaPorId(1);

        Mockito.verify(apiRestFeign, Mockito.times(1)).listarCategorias();

        Assertions.assertEquals(response.getId(), 1);
    }

}
