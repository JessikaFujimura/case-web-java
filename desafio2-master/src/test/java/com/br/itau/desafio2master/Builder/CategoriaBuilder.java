package com.br.itau.desafio2master.Builder;

import com.br.itau.desafio2master.Model.Categoria;

public class CategoriaBuilder {

    private Categoria categoria;

    private CategoriaBuilder(){}

    public static CategoriaBuilder umaCategoria(){
        CategoriaBuilder builder = new CategoriaBuilder();
        builder.categoria = new Categoria();
        builder.categoria.setId(1);
        builder.categoria.setNome("nome");
        return builder;
    }

    public Categoria build(){
        return categoria;
    }
}
