package com.br.itau.desafio2master.ApiRest;

import com.br.itau.desafio2master.Model.Categoria;
import com.br.itau.desafio2master.Model.Lancamento;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ApiRestFeignTest {

    @Autowired
    private ApiRestFeign apiRestFeign;

    @Test
    public void testListarLancamentos() throws Exception {

      List<Lancamento> list = apiRestFeign.listarLancamentos();
      Assertions.assertNotNull(list);

    }

    @Test
    public void testListarCategoria() throws Exception {

        List<Categoria> list = apiRestFeign.listarCategorias();
        Assertions.assertNotNull(list);

    }


}
