package com.br.itau.desafio2master.Controller;

import com.br.itau.desafio2master.ApiRest.ApiRestFeign;
import com.br.itau.desafio2master.Builder.LancamentoBuilder;
import com.br.itau.desafio2master.Model.Lancamento;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class LancamentoControllerTest{

    @InjectMocks
    private LancamentoController controller;

    @Mock
    private ApiRestFeign apiRestFeign;


    @Test
    public void testListarTodosOsLancamentosSemFiltro(){
        List<Lancamento> expectedResponse = List.of(LancamentoBuilder.umLancamento().build());
        Mockito.when(apiRestFeign.listarLancamentos()).thenReturn(expectedResponse);

        controller.getLancamento(null);

        Mockito.verify(apiRestFeign, Mockito.times(1)).listarLancamentos();
    }

    @Test
    public void testListarTodosOsLancamentosDeMesmaCategoria(){
        List<Lancamento> expectedResponse = List.of(LancamentoBuilder.umLancamento().build(),
                LancamentoBuilder.umLancamento().build());
        Mockito.when(apiRestFeign.listarLancamentos()).thenReturn(expectedResponse);

        List<Lancamento> response = controller.getLancamento(1);

        Mockito.verify(apiRestFeign, Mockito.times(1)).listarLancamentos();

        Assertions.assertEquals(response.size(), 2);
        Assertions.assertEquals(response.get(0).getCategoria(), 1);
    }


    @Test
    public void testObterUmLancamentoPorId(){
        List<Lancamento> lista = List.of(LancamentoBuilder.umLancamento().build(),
                LancamentoBuilder.umLancamento().build());
        Mockito.when(apiRestFeign.listarLancamentos()).thenReturn(lista);

        Lancamento response = controller.getLancamentoPorId(1);

        Mockito.verify(apiRestFeign, Mockito.times(1)).listarLancamentos();

        Assertions.assertEquals(response.getId(), 1);
    }

}
