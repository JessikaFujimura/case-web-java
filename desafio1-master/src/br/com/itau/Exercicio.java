package br.com.itau;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

import java.text.NumberFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Exercicio {

	public static void main(String[] args) {
		List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
		System.out.println("ORDENANDO OS GASTOS POR MÊS");
		ordenarGastosPorMes(lancamentos);

		System.out.println("\nLANÇAMENTOS DE MESMA CATEGORIA ESCOLHIDA");
		imprimirLancamentosDeMesmaCategoria(lancamentos, 6);

		System.out.println("\nVALOR TOTAL DA FATURA DE MÊS ESCOLHIDO");
		mostrarTotalDaFaturaDeUmMes(lancamentos, 1);

	}

	private static void mostrarTotalDaFaturaDeUmMes(List<Lancamento> lancamentos, int mes) {
		List<Lancamento> lancamentosDoMes = lancamentos.stream().filter(i -> i.getMes().equals(mes))
				.collect(Collectors.toList());
		Double total = lancamentosDoMes.stream().flatMapToDouble(i -> DoubleStream.of(i.getValor())).reduce(0, Double::sum);
		Locale locale = new Locale("pt", "BR");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		System.out.println("O total da fatura do mes " + mes + " é igual a :" + numberFormat.format(total));
	}

	private static void imprimirLancamentosDeMesmaCategoria(List<Lancamento> lancamentos, int categoria) {
		List<Lancamento> mesmaCategoria = lancamentos.stream().filter(i -> i.getCategoria().equals(categoria))
				.collect(Collectors.toList());
		for (Lancamento l: mesmaCategoria){
			System.out.println(l);
		}
	}

	private static void ordenarGastosPorMes(List<Lancamento> lancamentos){
		lancamentos.sort(new Comparator<Lancamento>() {
			@Override
			public int compare(Lancamento o1, Lancamento o2) {
				return o1.getMes().compareTo(o2.getMes());
			}
		});
		Locale locale = new Locale("pt", "BR");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		for (Lancamento l: lancamentos){
			System.out.println("Mes:" + l.getMes() + " - valor: " + numberFormat.format(l.getValor()));
		}
	}
}
