import { Routes, Route, BrowserRouter } from "react-router-dom";
import ListaGeral from "./pages/ListaGeral";
import ListaPorMes from "./pages/ListaPorMes";
import ListaPorCategoria from "./pages/ListPorCategorias";


function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<ListaGeral />} />
        <Route path="categoria" element={<ListaPorCategoria />} />
        <Route path="mes" element={<ListaPorMes />} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
