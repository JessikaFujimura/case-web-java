import Table from "../components/Table";
import NavBar from "../components/NavBar";
import axios from "axios";
import { useEffect, useState } from "react";
import { BASE_URL } from "../urlApi";

export default function ListaGeral() {
  const [lancamentos, setLancamentos] = useState([]);

  async function listarLancamentos() {
    await axios.get(`${BASE_URL}/lancamentos`).then((response) => {
      setLancamentos(response.data);
    });
  }

  useEffect(() => {
    listarLancamentos();
  }, [lancamentos]);

  return (
    <>
      <NavBar num="1"/>
      <section className="pl-3 pr-3 section">
        <h4 className="ml-1">Listagem de todos os lançamentos registrados</h4>
        <Table itens={lancamentos} />
      </section>
    </>
  );
}
