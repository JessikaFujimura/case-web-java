import Table from "../components/Table";
import NavBar from "../components/NavBar";
import axios from "axios";
import { useEffect, useState } from "react";
import { BASE_URL } from "../urlApi";

export default function ListaPorCategoria() {
  const lancamentosPorCategoria = [];
  const [lancamentos, setLancamentos] = useState([]);
  const [categorias, setCategorias] = useState([]);

  async function listarLancamentos() {
    await axios.get(`${BASE_URL}/lancamentos`).then((response) => {
      agruparPorCategoria(response.data);
    });
  }

  async function listarCategorias() {
    await axios.get(`${BASE_URL}/categorias`).then((response) => {
      setCategorias(response.data);
    });
  }

  function agruparPorCategoria(lancamentos) {
    categorias.forEach(el => {
      const lancamentoFiltradosPorCategoria = lancamentos.filter((i) => i.categoria === el.id);
      if(lancamentoFiltradosPorCategoria.length > 0) {
          const valorDoMes = lancamentoFiltradosPorCategoria.reduce(
            (total, item) => total + Number(item.valor),
            0
          );
    
          lancamentosPorCategoria.push({
            id: el.id,
            mes_lancamento: "diversos",
            valor: valorDoMes,
            origem: "diversas",
            categoria: el.nome,
          });
      }
    });
    setLancamentos(lancamentosPorCategoria);
  }

  useEffect(() => {
    listarCategorias()
    listarLancamentos();
  }, [lancamentos]);

  return (
    <>
      <NavBar num="3" />
      <section className="pl-3 pr-3 section">
        <h4 className="ml-1 pt-1">Listagem dos gastos por categoria</h4>
        <Table itens={lancamentos} />
      </section>
    </>
  );
}
