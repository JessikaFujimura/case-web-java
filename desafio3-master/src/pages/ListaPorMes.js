import Table from "../components/Table";
import NavBar from "../components/NavBar";
import axios from "axios";
import { useEffect, useState } from "react";
import { BASE_URL } from "../urlApi";

export default function ListaPorMes() {
  const [lancamentos, setLancamentos] = useState([]);
  let lancamentosPorMes = [];

  async function listarLancamentos() {
    await axios.get(`${BASE_URL}/lancamentos`).then((response) => {
      agruparPorMes(response.data);
    });
  }

  function agruparPorMes(lancamentos) {
    for (let m = 1; m <= 12; m++) {
      const lancamentoDoMes = lancamentos.filter((i) => i.mes_lancamento === m);
      if(lancamentoDoMes.length > 0) {
          const valorDoMes = lancamentoDoMes.reduce(
            (total, item) => total + Number(item.valor),
            0
          );
    
          lancamentosPorMes.push({
            id: m,
            mes_lancamento: m,
            valor: valorDoMes,
            origem: "diversas",
            categoria: "diversas",
          });
      }
    }
    setLancamentos(lancamentosPorMes);
  }

  useEffect(() => {
    listarLancamentos();
  }, []);

  return (
    <>
      <NavBar num="2"/>
      <section className="pl-3 pr-3 section">
        <h4 className="ml-1">Listagem dos gastos por mês</h4>
        <Table itens={lancamentos} />
      </section>
    </>
  );
}
