export default function Table({ itens }) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
            <th scope="col">Mês</th>
            <th scope="col">Origem</th>
            <th scope="col">Valor</th>
            <th scope="col">Categoria</th>
          </tr>
      </thead>
      <tbody>
        {itens.map((i) => (
          <tr key={i.id}>
            <th scope="row">{i.mes_lancamento}</th>
            <td>{i.origem}</td>
            <td>
              {i.valor.toLocaleString("pt-br", {
                style: "currency",
                currency: "BRL",
              })}
            </td>
            <td>{i.categoria}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
