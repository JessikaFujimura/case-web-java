import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {} from "../App.css";

export default function NavBar({num}) {
  const [menu,setMenu] = useState([
    {
      num: 1,
      item: "Lista Geral",
      path: "/",
      active: false
    },
    {
      num: 2,
      item: "Gasto por mês",
      path: "/mes",
      active: false

    },
    {
      num: 3,
      item: "Gastos por Categoria",
      path: "/categoria",
      active: false

    },
  ]);


  function activateLabel(num){
    setMenu(menu.map(i => i.num === Number(num) ? {...i, active: true} : i))
  }


  useEffect(() => {
    activateLabel(num)
  }, [menu]);


  return (
    <>
      <ul className="nav justify-content-center navbar-style fixed">
        {menu.map((i) => i.active ? <li key={i.num} className="nav-item">
            <Link className="nav-link active" to={i.path}>
              {i.item}
            </Link>
          </li>
          :
          <li key={i.num} className="nav-item">
            <Link className="nav-link" to={i.path}>
              {i.item}
            </Link>
          </li>)
          }
      </ul>
    </>
  );
}
